'''
3. Take two numbers from users and print the sum of those numbers
    if the sum is even.
    Input1: num1 = 10
    Input2: num2 = 20
    Output: 30 is Even
'''

num1 = int(input("Enter a number 1 :"))
num2 = int(input("Enter a number 2 :"))

sum = num1 + num2
if(sum % 2 == 0):
    print(sum,"is even")
    