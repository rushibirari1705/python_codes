
'''
2. WAP to determine whether entered angles define a right-angled triangle.
Take three values of angle from the user.

Input1: angel1 = 90
Input2: angle2 = 90
Input3: angle3 = 90
Output:
It is not a right-angle triangle

Input1: angel1 = 90
Input2: angle2 = 60
Input3: angle3 = 30
Output:
It is a right-angle triangle


'''

ag1 = int(input("Enter angle 1 : "))
ag2 = int(input("Enter angle 2 : "))
ag3 = int(input("Enter angle 3 : "))

if ag1 == (ag2 + ag3):
    print("Right angle Triangle")
elif ag2 == (ag1 + ag3):
    print("Right angle Triangle")
elif ag3 == (ag2 + ag1):
    print("Right angle Triangle")
else:
    print("Not right angle Triangle")