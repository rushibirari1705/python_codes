
'''
9. Take Two character from user check if the ascii value both of character are
    odd then print the sum of ascii values of character
    #Input: char1 = 'A'
            char2 = 'C'
    #Output: 132
    #Input: char1 = 'a'
            char2 = 'b'
    #Output:No Output

'''

ch1 = input("Enter character 1 : ")
ch2 = input("Enter character 2 : ")

if(ord(ch1) % 2 == 1 and ord(ch2) % 2 == 1):
    sum = ord(ch1)+ ord(ch2)
    print(sum)

