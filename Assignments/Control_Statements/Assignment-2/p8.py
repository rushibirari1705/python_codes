
'''
8. Take single character from user check if the ascii value of character is
Even the print character.
    #Input char1 = 'B'
    #Output: B
    #Input char1 = 'C'
    #Output: No Output

'''

ch = input("Enter character :")

if( ord( ch ) % 2 == 0 ):
    print( ch )