

# 10. print Pattern :
'''
   1   3   5   9
   5   7   9   11
   9   11  13  15
   13  15  17  19
   
    
'''

rows = int(input("Enter the rows : "))
num1 = 1
for i in range(rows):
    num = num1
    for j in range(rows):
        print(num, end= "   ")
        num = num + 2
        
    num1 = num1 + 4
    print()