

# 9. print Pattern :
'''
   1   3   5 
   3   5   7
   5   7   9 
   
    
'''

rows = int(input("Enter the rows : "))
num1 = 1
for i in range(rows):
    num = num1
    for j in range(rows):
        print(num, end= "   ")
        num = num + 2
        
    num1 = num1 + 2
    print()