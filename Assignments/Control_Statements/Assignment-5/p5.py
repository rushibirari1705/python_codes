# 5 . print Pattern :
'''
   1   2   3   4
   2   3   4   5
   3   4   5   6
   4   5   6   7
    
    
'''

rows = int(input("Enter the rows : "))
num = 1
for i in range(rows):
    num2 = num
    for j in range(rows):
        print(num2,end = "    ")
        num2 = num2 + 1
        
    num = num + 1   
    print()