

#  3 . print Pattern :
'''
    1    1    1    1
    2    2    2    2
    3    3    3    3
    4    4    4    4
    
    
'''

rows = int(input("Enter the rows : "))
num = 1
for i in range(rows):
    for j in range(rows):
        print(num,end = "  ")
        
    num = num + 1   
    print()