
'''
8 .WAP to prints numbers which are divisible by 3 and 5 both in a given range.
        Input:
        Start:15
        End: 50
        Output:
        15 30 45


'''

start = int(input("Enter the start : "))
end = int(input("Enter the end : "))

for i in range(start, end + 1):
        if i % 3 == 0 and i % 5 == 0:
            print(i , end = " ")