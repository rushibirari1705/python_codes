
'''
6 . WAP to print all the ASCII values from a given character range.
    Input :
    Enter the start of range : A
    Enter end of range: Z
    Output :
    ASCII value of A is 65
    ASCII value of B is 66
    ASCII value of C is 67
    .
    .
    .
    ASCII value of Y is 89


'''

ch1 = (input("Enter the start character : "))
ch2 = (input("Enter the end character : "))

start = ord(ch1)
end = ord(ch2)

if start < 1 or end < 1:
    print("Wrong input")
else:
    for i in range(start, end):
        print("The ASCII value ", chr(i) , " is ",i)