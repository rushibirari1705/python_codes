
'''
7 .WAP that prints all Positive numbers from a given range.
    Input:
    Start: -7
    End: 8
    Output:
    1 2 3 4 5 6 7

'''

start = int(input("Enter the start : "))
end = int(input("Enter the end : "))

for i in range(1, end):
        print(i)