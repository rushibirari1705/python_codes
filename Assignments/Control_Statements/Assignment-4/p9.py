
'''

9 . WAP to print the count of all negative numbers from a given range
    Input:
    Start: -15
    End: 50
    Output:
    15

'''
start = int(input("Enter the start : "))
end = int(input("Enter the end : "))
cnt = 0
for i in range(start, end + 1):
        if i < 0:
          cnt = cnt + 1

print(cnt)