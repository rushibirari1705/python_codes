
'''
1 . WAP to print numbers from a given range.
Input :
Start = 100;
End = 110;
Output:
100 101 103 104 105 106 107 108 109

'''

start = int(input("Enter the start : "))
e = int(input("Enter the end : "))

for i in range(start, e ):
    print(i , end = " " )