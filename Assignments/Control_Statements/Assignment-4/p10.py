
'''
10 .WAP program to calculate and print the product of the count of odd
        numbers within a given range.
    Input:
    Start: 1
    End: 11
    Output:
    3125

'''
start = int(input("Enter the start : "))
end = int(input("Enter the end : "))
cnt = 0
prod = 1
for i in range(start, end + 1):
        if i % 2 != 0:
          cnt += 1
          prod *= i

print(prod)