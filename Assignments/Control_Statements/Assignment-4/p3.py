
'''
3 . WAP to print the sum of all numbers from a given range.
    Input:
    Start:1
    End: 10
    Output:
    45

'''
start = int(input("Enter the start : "))
end = int(input("Enter the end : "))

sum = 0
for i in range(start, end ):
   sum += i

print(sum)