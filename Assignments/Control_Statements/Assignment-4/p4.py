'''
4 .WAP to print all the character values of the given ASCII value range from the user
    Input :
    Enter the start of range : 1
    Enter end of range: -2
    Output :
    Wrong input

    Input :
    Enter start of range : 65
    Enter end of range : 67

    Output :

    The character of ASCII value 65 is A.
    The character of ASCII value 66 is B.
    The character of ASCII value 67 is C.
    .
    .
    .
    The character of ASCII value 89 is Y.

'''

start = int(input("Enter the start : "))
end = int(input("Enter the end : "))

if start < 1 or end < 1:
    print("Wrong input")
else:
    for i in range(start, end + 1):
        print("The character for ASCII value ", i , " is ",chr(i))
