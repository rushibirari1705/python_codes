
# 4. WAP to check the given number is divisible by 5 or not

x = 55

if(x % 5 == 0):
    print(x ," is divisible by 5")
else:
    print(x ," is not divisible by 5")